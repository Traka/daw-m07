<?php
/**
 * Created by PhpStorm.
 * User: Azurale
 * Date: 09/11/2016
 *
 * Time: 17:31
 */
session_start();
if(!isset($_SESSION['Usuari'])){
    die("No pots entrar a la pagina degut a que no estas conectat");
}else{
    $usuari = $_SESSION['Usuari'];
}
if(isset($_POST['numero'])){
    $num = $_POST['numero'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Comprovar divisors</title>
</head>
<body>
<div style="margin: 30px 10%;">
    <form action="comprovar_divisors.php" method="post" id="myform" name="myform">

        <h2>Numero a comprovar:</h2>
        <input type="text" value="" size="30" maxlength="100" name="numero" id="" /><br /><br />
        <button id="mysubmit" type="submit">Comprovar</button><br /><br />
    </form>

    <h2>
        <?php
            if(isset($num)){
                if(isPrime($num)){
                    echo "El número " . $num . " és primer";
                }else{
                    echo "El número " . $num . " no és primer i els seus divisors son: ";
                    divisors($num);
                }
            }

        ?>
    </h2><br/>

        <?php
        function isPrime($num) {
            $cont=0;
            for($i=2;$i<=$num;$i++){
                if($num%$i==0){
                    if(++$cont>1)
                        return false;
                }
            }
            return true;
        }
        function divisors($num){
            for($i = 1; $i <= $num; $i ++) {
                if($i == $num){
                    echo " i ell mateix";
                }elseif ($i == 1){
                    echo $i;
                }elseif($num % $i == 0){
                    echo ", ".$i;
                }
            }
        }
        ?>
    <h3><a href="menu.php">Torna al menu</a></h3>
</div>
</body>
</html>

