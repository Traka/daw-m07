<?php
/**
 * Created by PhpStorm.
 * User: Azurale
 * Date: 09/11/2016
 * Time: 17:19
 */
session_start();
$usuaris = array( "pere"=>"secret", "joan"=>"rambo", "marta"=>"ramba");
$uSER = $_POST['UserName'];
$PASS = $_POST['Password'];
$CORRECTE =  isset($_POST['UserName'])
    && isset($_POST['Password'])
    && isset($usuaris[$uSER])
    && $usuaris[$uSER] == $PASS;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Pagina Temporal</title>
</head>
<body>
<div style="margin: 30px 10%;">
    <?php
    if($CORRECTE){
        $_SESSION['Usuari'] = $uSER;
        header('Location: menu.php');
    }else{
        setcookie("errors", "Usuari incorrecte");
        header('Location: formulari_login.php');
    }
    ?>
</div>
</body>
</html>