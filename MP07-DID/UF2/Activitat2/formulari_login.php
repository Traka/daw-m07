<?php
/**
 * Created by PhpStorm.
 * User: Azurale
 * Date: 09/11/2016
 *
 * Time: 17:31
 */
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Exemple de formulari</title>
</head>
<body>
<div style="margin: 30px 10%;">
    <form action="processa_login.php" method="post" id="myform" name="myform">
        <h1>Autentificació</h1>
        <h2 style="color:red;">  <?php
            if(isset($_COOKIE["errors"])){
                echo ($_COOKIE["errors"]);
                setcookie("errors",null, -1);
            }
            ?>
        </h2>
      <br/>
        <label>Ususari</label>
        <input type="text" value="" size="30" maxlength="100" name="UserName" id="" /><br /><br />
        <label>Contraseña</label>
        <input type="password" value="" size="30" maxlength="100" name="Password" id="" /><br /><br />
        <button id="mysubmit" type="submit">Entra</button><br /><br />
    </form>
</div>
</body>
</html>
