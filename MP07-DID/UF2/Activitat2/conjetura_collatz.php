<?php
/**
 * Created by PhpStorm.
 * User: Azurale
 * Date: 09/11/2016
 *
 * Time: 17:31
 */
session_start();
if(!isset($_SESSION['Usuari'])){
    die("No pots entrar a la pagina degut a que no estas conectat");
}else{
    $usuari = $_SESSION['Usuari'];
}
if(isset($_POST['numero'])){
    $num = $_POST['numero'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Comprovar divisors</title>
</head>
<body>
<div style="margin: 30px 10%;">
    <form action="conjetura_collatz.php" method="post" id="myform" name="myform">

        <h2>Numero a calcular amb la conjetura collatz:</h2>
        <input type="text" value="" size="30" maxlength="100" name="numero" id="" /><br /><br />
        <button id="mysubmit" type="submit">Comprovar</button><br /><br />
    </form>

    <h2>
        <?php
            if(isset($num)) {
                echo "La conjetura collatz de " . $num . " és:";
            }
        ?>
    </h2>

    <h3>
        <?php
            if(isset($num)) {
                $count = 0;
                $mesAlt = 0;
                while($num <> 1){
                    if(($num % 2) == 0){
                        $num = $num / 2;
                        $count += 1;
                        if($mesAlt < $num){
                            $mesAlt = $num;
                        }
                        echo $num." ";
                    }else{
                        $num = ($num *3) + 1;
                        $count += 1;
                        if($mesAlt < $num){
                            $mesAlt = $num;
                        }
                        echo $num." ";
                    }
                }
                echo "després de ".$count." iteracions i arribant a un màxim de ".$mesAlt."";
            }
        ?>
    </h3>
    <h3><a href="menu.php">Torna al menu</a></h3>

</div>
</body>
</html>