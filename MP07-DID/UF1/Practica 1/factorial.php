<?php

$numeros = array(1, 2, 3, 4, 5, 6, 7);

print_r(FactorialArray($numeros));

/**
 * Amb aquesta funcio el que fa es comprovar si el parametre d'entrada es una array, posteriorment si es cert crear
 * una array la qual sera buida, i posteriorment amb un foreach anirem executant la funcio CalculaFactorial per tots
 * els camps de l'array que ens han entrat.
 * @param $array parametre que ens han entrat.
 * @return array|bool retorna l'array modificada en cas de que el parametre d'entrada fos una array o retorna un false
 * en cas de que fos un altre tipus de parametre.
 */
function FactorialArray($array) {
    if(is_array($array)){
        $resultat = array();
        $compt = 0;
        foreach ($array as $valor){

            $resultat[$compt++] = CalculaFactorial($valor);

        }
        return $resultat;
    }else{
        return false;
    }

}

/**
 * Comprova si el valor entrat es un int i posteriorment calcula el factorial de forma recursiva. 
 * @param $valor valor entrat com a parametre.
 * @return bool|int Retorna el factorial del valor que ens han entrat o un false en cas de que no fos un int
 */
function CalculaFactorial($valor){
    if (is_int($valor)){
        if ($valor < 2) {
            return 1;
        } else {
            return ($valor * CalculaFactorial($valor-1));
        }
    }else{
        return false;
    }

}

?>