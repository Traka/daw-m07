<?php

$matriu = crearMatriu(4);

mostrarMatriu($matriu);
mostrarMatriu(mostrarMatriuGirada($matriu));
/**
 * Aquesta funcio el que fa es crear una array multidimensional amb una grandaria equivalent al
 * numero entrat per l'usuari i anira emplenantla seguint el següent patro:
 * Linea diagonal de *
 * La part esquerre de la linea de * sera emplenada per randoms entre 10 i 20
 * La part dreta de la linea de * sera emplenada per el valor de columna + fila.
 * @param $num numero entrar per l'usuari que estableix la grandaria de l'array.
 */
function crearMatriu($num){
    $aQuadrada = array();
    for ($i = 0; $i <= $num; $i++){
        for($j = 0; $j <= $num; $j++){

            if($j < $i){
                $aQuadrada[$i][$j] = rand(10, 20);
            }else if($i == $j){
                $aQuadrada[$i][$j] = "*";
            }else{
                $aQuadrada[$i][$j] = $i + $j;
            }

        }
    }
    return $aQuadrada;
}


function mostrarMatriu($aQuadrada){
    print("<table>");
    for ($i = 0; $i <= count($aQuadrada); $i++){
        print("<tr>");
        for($j = 0; $j <= count($aQuadrada); $j++){
            print("<td>".$aQuadrada[$i][$j]."</td>");
        }
        print("</tr>");
    }
    print("</table>");
}

function mostrarMatriuGirada($aQuadrada){
    $aQuadradaGirada = array();
    for ($i = 0; $i <= count($aQuadrada); $i++){
        for($j = 0; $j <= count($aQuadrada); $j++){
            $aQuadradaGirada[$j][$i] =$aQuadrada[$i][$j];
        }
    }
  return $aQuadradaGirada;
}

?>