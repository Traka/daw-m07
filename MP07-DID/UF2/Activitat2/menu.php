<?php
/**
 * Created by PhpStorm.
 * User: Azurale
 * Date: 16/11/2016
 * Time: 17:08
 */
session_start();
if(!isset($_SESSION['Usuari'])){
    die("No pots entrar a la pagina degut a que no estas conectat");
}else{
    $usuari = $_SESSION['Usuari'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Exemple de formulari</title>
        </head>
    <body>
        <h1>
            <?php
                echo "Benvingut " .$usuari; ?>
        </h1>
        <h2>Selecciona una opció</h2>
        <ul>
            <li><a href="comprovar_divisors.php">Comprovar divisors</a></li>
            <li><a href="conjetura_collatz.php">Conjetura collatz</a></li>
            <li><a href="sortir.php">sortir</a></li>
        </ul>
    </body>
</html>
